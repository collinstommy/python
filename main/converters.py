from datetime import date, time, datetime
import re


def tuple_to_date(date_tuple):
    return date(date_tuple[0], date_tuple[1], date_tuple[2])

def tuple_to_time(date_tuple):
    return time(date_tuple[3], date_tuple[4])

def extract_time(text):
    match = re.search(r'(([01]\d|2[0-3]):([0-5]\d)|24:00)', text)
    if(match):
        time_text =  match.group(1)
        time_from_text = datetime.strptime(time_text, "%H:%M")
        return time(time_from_text.hour, time_from_text.minute), time_text
    return None