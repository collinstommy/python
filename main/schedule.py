import argparse
import httplib2
import os
import json

from apiclient import discovery
from oauth2client import file
from oauth2client import client
from oauth2client import tools
from main.excel import get_schedule
from flask import current_app
from pytz import timezone
import datetime
from fileinput import filename

# Parser for command-line arguments.
parser = argparse.ArgumentParser(
    description=__doc__,
    formatter_class=argparse.RawDescriptionHelpFormatter,
    parents=[tools.argparser])

pacific = timezone('US/Pacific-New')
CLIENT_SECRETS = os.path.join(os.path.dirname(__file__), 'client_secrets.json')
FLOW = client.flow_from_clientsecrets(CLIENT_SECRETS,
  scope=[
      'https://www.googleapis.com/auth/calendar',
      'https://www.googleapis.com/auth/calendar.readonly',
    ],
    message=tools.message_if_missing(CLIENT_SECRETS))

def remove_quotes(string):
    if string.startswith('"') and string.endswith('"'):
        return string[1:-1]
    return string

def create_events(schedule, service, summary, email):
    
    events_created = []   
    for shift_start in schedule:
            shift_start = pacific.localize(shift_start)
            end = shift_start + datetime.timedelta(hours=8)
            id = create_event(service, summary, shift_start, end, email) 
            events_created.append({'id' : id, 'start' : shift_start, 'end' : end})
        
    return events_created         

def create_event(service, summary, start, end, email):

    event = {
        'summary':summary, 
        'start':{
            'dateTime':remove_quotes(json.dumps(start, default=date_handler))}, 
        
        'attendees':[
            {
                'email':email}],
        'end':{
            'dateTime': remove_quotes(json.dumps(end, default=date_handler))},
             }
    
    created_event = service.events().insert(calendarId=email, body=event).execute()
    current_app.logger.info("Event created. ID: {id}  Email: {email_address}".format(id=created_event['id'], email_address=email))
    
    print "Success! Event created"
    print created_event['id']
    
    return created_event['id']
    
def date_handler(obj):
    return obj.isoformat() if hasattr(obj, 'isoformat') else obj  

def construct_api_service(email):
    storage_file_name = email + '_credentials.dat'
    storage = file.Storage(storage_file_name)
    credentials = storage.get()
    
    argv = None 
    parser = argparse.ArgumentParser(
    description=__doc__,
    formatter_class=argparse.RawDescriptionHelpFormatter,
    parents=[tools.argparser])
    flags = parser.parse_args(argv)
    
    if credentials is None or credentials.invalid:
        credentials = tools.run_flow(FLOW, storage, flags)
    
    http = httplib2.Http()
    http = credentials.authorize(http)
    
    return discovery.build('calendar', 'v3', http=http)

def save_to_calendar(name, email, filepath):
    
    current_app.logger.debug(filename)
    service = construct_api_service(email)
    schedule = get_schedule(name, filepath)
    summary = 'Mallard'
    
    try:
        result = create_events(schedule, service, summary, email)
      
    except client.AccessTokenRefreshError:
        print ("The credentials have been revoked or expired, please re-run"
      "the application to re-authorize")

    return result