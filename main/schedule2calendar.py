import logging
import os

from flask import Flask
from flask.templating import render_template
from flask import request
from schedule import save_to_calendar
from logging.handlers import RotatingFileHandler
from logging import Formatter
from werkzeug.utils import secure_filename
from custom_exceptions import NameNotFoundException

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = 'uploads/'
app.config['ALLOWED_EXTENSIONS'] = set(['xls'])


handler = RotatingFileHandler('scedule2calendar.log', maxBytes=10000, backupCount=5)
handler.setFormatter(Formatter(
    '%(asctime)s %(levelname)s: %(message)s '
    '[in %(pathname)s:%(lineno)d]'
))
app.logger.addHandler(handler)
handler.setLevel(logging.DEBUG)

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in app.config['ALLOWED_EXTENSIONS']

@app.route('/')
def information_form():
    return render_template('calendar.html')

@app.route('/save_calendar', methods=['POST'])
def calendar_save():
    errors = []
    filepath = None
    if request.method == 'POST':
        schedule_file = request.files['excel_file']
        name = request.form['first_name']
        email = request.form['gmail_address']
        if schedule_file and allowed_file(schedule_file.filename):
            filename = secure_filename(schedule_file.filename)
            filepath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            schedule_file.save(filepath)
        else:
            errors.append("File type must be .xls")
            
    events = []         
    if not errors:     
            events = save_to_calendar(name, email, filepath)
    return render_template("results.html", events = events, errors = errors)
   
            
if __name__ == '__main__':
    app.run()