'''
Created on Jun 19, 2014

@author: thomascollins
'''

class Event(object):
    '''
    classdocs
    '''

    def __init__(self, date, time=None, details=None):
        self.date = date
        self.time = time
        self.details = details
        
    def __eq__(self, obj):
            return self.date == obj.date and self.time == obj.time and self.details == obj.details
        
        