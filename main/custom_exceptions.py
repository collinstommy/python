'''
Created on Jun 19, 2014

@author: thomascollins
'''

class NotFoundException(ValueError):
    pass

from werkzeug.exceptions import HTTPException

class NameNotFoundException(HTTPException):
    code = 500
    description = 'Name not found on Schedule. Please check spelling.'