'''
Created on Jun 5, 2014

@author: thomascollins
'''
from datetime import date,datetime,time
from xlrd import open_workbook,xldate_as_tuple
from custom_exceptions import NameNotFoundException
from main.Event import Event
from main.converters import tuple_to_date, tuple_to_time, extract_time
from string import strip

def get_raw(name, filepath):
    wb = open_workbook(filepath)
    datemode = wb.datemode
    
    employee_row_num, sheet = _find_employee_row(wb,datemode,name)
    
    events = []
    for day in range(1,8):
        date = tuple_to_date(xldate_as_tuple(sheet.cell(4, day).value,datemode))
        try:
            time = tuple_to_time(xldate_as_tuple(sheet.cell(employee_row_num, day).value, datemode))
            events.append(Event(date, time))
        except ValueError:
            cell_value = sheet.cell(employee_row_num, day).value
            time, time_text = extract_time(cell_value)
            addition_details = strip(cell_value.replace(time_text, ""))
            events.append(Event(date, time, addition_details))
    return events

def get_schedule(name, filepath):
    wb = open_workbook(filepath)

    datemode = wb.datemode
    
    employee_row_num, sheet = _find_employee_row(wb,datemode,name)
    if employee_row_num is None:
        raise NameNotFoundException
    
    dates = []
    times = []
    for day in range(1,8):
        try:
            date = xldate_as_tuple(sheet.cell(4, day).value,datemode)
            time = xldate_as_tuple(sheet.cell(employee_row_num, day).value,datemode)
            dates.append(date)
            times.append(time)
        except ValueError:
            print "Wahey off"
        
    date_times = []
    for x in range(0,len(dates)):
        date = datetime(dates[x][0],dates[x][1],dates[x][2],times[x][3],times[x][4], 0)
        date_times.append(date)
    
    return date_times
                
def _find_employee_row(workbook, datemode, name):
    
    for sheet in workbook.sheets():
        for row in range(sheet.nrows):
            for col in range(sheet.ncols):
                val = sheet.cell(row, col).value
                if val == name:
                    return row, sheet
    return None

def _find_date_row(sheet,datemode):
    print xldate_as_tuple(sheet.cell(4,1).value,datemode)
    print xldate_as_tuple(sheet.cell(4,2).value,datemode)
    
    return sheet.row(4)

