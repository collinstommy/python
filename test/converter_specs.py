import unittest
from main.converters import extract_time
from datetime import time

class TimeExtracted(unittest.TestCase):
    
    expected = time(19, 0)
    
    def setUp(self):
        self.given_some_text()
        self.when_extracted()
        
    def given_some_text(self):
        self.some_text = "19:00 TR"   
    
    def when_extracted(self):
        self.result = extract_time(self.some_text)
        
    def test_correct_time_extracted(self):
        self.assertEqual(self.result, self.expected, "Incorrect time returned")