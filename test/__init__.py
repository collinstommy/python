import unittest
from main.excel import get_schedule, get_raw
from datetime import date,datetime,time
from main.Event import Event
import datetime

class CorrectDateObjectsReturned(unittest.TestCase):
    def setUp(self):
        self.given_filename()
        self.given_name()
        self.result = get_raw(self.name, self.filename)
        self.expected_result = self.expected()
        
    
    def given_filename(self):
        self.filename = "Sample Schedule.xls"
    
    def given_name(self):
        self.name = "Thomas"
            
    def expected(self):
        events = []
        events.append(Event(date(2014, 6, 6), time(10,0), "TR")) #Fri
        events.append(Event(date(2014, 6, 7), time(18,0))) #Sat
        events.append(Event(date(2014, 6, 8), time(19,0))) #Sun
        events.append(Event(date(2014, 6, 9), time(18,0))) #Mon
        events.append(Event(date(2014, 6, 10), time(9,30))) #Tue
        events.append(Event(date(2014, 6, 12), time(10,0)))   #Thurs     
        return events
       
    def test_correct_output(self):
        self.assertEqual(self.result, self.expected(), "Schedule does not match")
        
    def test_number_of_days(self):
        self.assertEqual(len(self.result), 7, "Incorrect number of days expected")


class CorrectDateReturned(unittest.TestCase):
    def setUp(self):
        self.given_filename()
        self.given_name()
        self.result = get_schedule(self.name, self.filename)
        self.expected_result = self.expected()
        
    
    def given_filename(self):
        self.filename = "Sample Schedule.xls"
    
    def given_name(self):
        self.name = "Thomas"
            
    def expected(self):
        datetimes = []
        datetimes.append(datetime.datetime(2014, 6, 6, 10, 0))
        datetimes.append(datetime.datetime(2014, 6, 7, 18, 0))
        datetimes.append(datetime.datetime(2014, 6, 8, 19, 0))
        datetimes.append(datetime.datetime(2014, 6, 9, 18, 0))
        datetimes.append(datetime.datetime(2014, 6, 10, 9, 30))
        datetimes.append(datetime.datetime(2014, 6, 11, 10, 30))
        datetimes.append(datetime.datetime(2014, 6, 12, 10, 0))
        return datetimes
       
    def _correct_output(self):
        self.assertEqual(self.result, self.expected(), "Schedule does not match")
        
    def _number_of_days(self):
        self.assertEqual(len(self.result), 7, "Incorrect number of days expected")
            
if __name__ == '__main__':
    unittest.main()